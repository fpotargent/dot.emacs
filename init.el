;;; init.el -- initialisation/startup code
;;; Commentary:
;;; Code:
;; Package manager
(require 'package)
(setq package-archives
      '(("gnu"    . "https://elpa.gnu.org/packages/")
        ("nongnu" . "https://elpa.nongnu.org/nongnu/")
        ("melpa"  . "https://melpa.org/packages/")
        ("reduce" . "https://reduce-algebra.sourceforge.io/reduce-ide/packages/")))
(package-initialize)

;; Configure use-package (built-in since 29.1)
(require 'use-package)
(setq use-package-compute-statistics t)
(setq use-package-always-ensure t)

;; Library of lisp configuration files (byte compiled)
(setq load-prefer-newer t)
(add-to-list 'load-path "~/.emacs.d/config/")
(byte-recompile-directory "~/.emacs.d/config/" 0)

(load-library "ui")         ; UI configuration
(load-library "general")    ; General editor behavior
(load-library "develop")    ; Development packages

;; Location of the customization file
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file 'noerror)

;;; init.el ends here
