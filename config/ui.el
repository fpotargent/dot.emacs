;;; ui --- UI configuration
;;; Commentary:
;;; Code:
(scroll-bar-mode      -1)
(tool-bar-mode        -1)
(tooltip-mode         -1)
(menu-bar-mode        -1)
(blink-cursor-mode    -1)
(line-number-mode      1)
(column-number-mode    1)
(size-indication-mode  1)
(save-place-mode       1)
(cua-selection-mode    t)
(set-fringe-mode       4)

(global-display-line-numbers-mode)

(setq inhibit-startup-screen t)
(setq ring-bell-function     'ignore)

;; Don't suspend frame on Ctrl-z (we still have "C-x C-z" available)
(global-unset-key (kbd "C-z"))

(setq scroll-margin 0
      scroll-conservatively 100000
      scroll-preserve-screen-position 1)

(add-to-list 'default-frame-alist '(font . "Source Code Pro:size=14"))
(setq-default line-spacing 0.15)

;; Note: run 'load-theme' interactively to get a list of available themes
;; material-theme: colors based on the Google Material design
;;                 package also contains a "material-light" theme
(use-package material-theme
  :config (load-theme 'material t))

;; Collection of icons
;; Note: requires specific fonts, these can be installed using
;;          (all-the-icons-install-fonts)
(use-package all-the-icons :if (display-graphic-p))

;; Doom modeline now uses the nerd-icons package
;; To install the necessary fonts use
;;          (nerd-icons-install-fonts)
(use-package nerd-icons :if (display-graphic-p))

;; Minimalistic modeline (from doom emacs)
(use-package doom-modeline
  :config (doom-modeline-mode 1))

(use-package ace-window
  :bind (("C-x o" . ace-window)
         ("M-o" . ace-window)))

;;; ui.el ends here
