;;; general --- General packages
;;; Commentary:
;;; Code:

;; create backup files in separate directory in .emacs.d
(setq backup-directory-alist '(("." . "~/.emacs.d/saves"))
      backup-by-copying   t
      delete-old-versions t
      kept-new-versions   6
      kept-old-versions   2
      version-control     t)

;; Utility packages
(use-package delight)
(use-package async
  :custom (async-bytecomp-allowed-packages '(all)))

;; Show popup of available keys with a partial key stroke
(use-package which-key
  :pin gnu
  :config (which-key-mode)
  :delight)

;; Enable smart parenthesis and hybrid regions of different pairs
(use-package smartparens
  :demand t
  :hook ((prog-mode text-mode markdown-mode) . smartparens-strict-mode)
  :config
  (require 'smartparens-config)
  (smartparens-global-mode)
  (show-smartparens-global-mode)
  (sp-use-smartparens-bindings)
  :custom
  (sp-show-pair-delay 0))

;; enable spelling checking
(defun flyspell-mode-with-maybe-nl-dictionary ()
  "Start flyspell using a dictionary appropriate for the buffer."
  (let ((found (seq-some (lambda (str) (string-prefix-p (buffer-name) str))
                         '("COMMIT_EDITMSG" "magit:"))))
    (unless found
      (setq-local ispell-local-dictionary "nl")))
  (flyspell-mode))

(use-package ispell
  :custom (ispell-dictionary "en_US")
  :hook ((prog-mode . flyspell-prog-mode)
         (text-mode . flyspell-mode-with-maybe-nl-dictionary)))

;; Enable vertico
(use-package vertico
  :config (vertico-mode))

;; Optionally use the `orderless' completion style.
(use-package orderless
  :config
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-ignore-case nil
        completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :config (savehist-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  ;; (defun crm-indicator (args)
  ;;   (cons (format "[CRM%s] %s"
  ;;                 (replace-regexp-in-string
  ;;                  "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
  ;;                  crm-separator)
  ;;                 (car args))
  ;;         (cdr args)))
  ;; (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))

;; Complete anything
(use-package company
  :custom
  (company-idle-delay 0.5)
  (company-show-numbers t)
  (company-tooltip-limit 10)
  (company-minimum-prefix-length 2)
  (company-tooltip-align-annotations t)
  :config (global-company-mode 1)
  :delight)

;; Generate blind text
(use-package lorem-ipsum
  :commands (lorem-ipsum-insert-list
             lorem-ipsum-insert-sentences
             lorem-ipsum-insert-paragraphs))

;; Outliner, note taking, ...
(use-package org
  :pin gnu
  :mode ("\\.org\\'" . org-mode)
  :bind (("C-c l" . org-store-link)
         ("C-c a" . org-agenda)
         ("C-c c" . org-capture))
  :config
  (require 'org-tempo)
  :custom
  (org-default-notes-file "~/notes.org")
  (org-use-speed-commands t)
  (org-support-shift-select t)
  (org-babel-load-languages
   (mapcar (lambda (v) (cons v t))
           '(dot emacs-lisp haskell latex scheme)))
  :custom-face
  (org-level-1               ((t . (:height 1.0))))
  (org-level-2               ((t . (:height 1.0))))
  (org-level-3               ((t . (:height 1.0))))
  (org-document-title        ((t . (:height 1.0))))
  (org-document-info         ((t . (:height 1.0))))
  (org-document-info-keyword ((t . (:height 1.0)))))

(use-package ox-pandoc :after org)

;;; general.el ends here
