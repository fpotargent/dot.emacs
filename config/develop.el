;;; develop.el -- development related configuration
;;; Commentary:
;;; Code:

;; Cleanup whitespace on save in programming modes
(use-package ws-butler
  :hook prog-mode)

;; Project management
(use-package projectile
  :bind-keymap ("C-c p" . projectile-command-map)
  :custom (projectile-require-project-root nil)
  :config
  (declare-function projectile-register-project-type "projectile")
  (projectile-register-project-type 'jonin '("Buildspec")
                                    :project-file "Buildspec"
                                    :compile "jonin ")
  (projectile-mode 1)
  :delight '(:eval (concat " [" (projectile-project-name) "]")))

;; Git wrapper
(use-package magit
  :bind (("C-x g"   . magit-status)
         ("C-x M-g" . magit-dispatch))
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package magit-gitflow
  :hook (magit-mode . turn-on-magit-gitflow))

;; Show uncommitted changes in the gutter
(use-package diff-hl
  :demand t
  :config (global-diff-hl-mode)
  :hook ((magit-pre-refresh . diff-hl-magit-pre-refresh)
         (magit-post-refresh . diff-hl-magit-post-refresh)))

;; Syntax checker
(use-package flycheck
  :config (global-flycheck-mode))

;; snippet support
(use-package yasnippet
  :config (yas-global-mode 1))

;; Reduce IDE
(use-package reduce-mode
  :ensure reduce-ide
  :mode "\\.red\\'")

;; Nim support
(use-package nim-mode
  :mode "\\.nim\\'")

;; Cobol
(use-package cobol-mode
  :mode "\\.cob\\'")

;; PowerShell scripts
(use-package powershell
  :mode ("\\.ps1\\'" . powershell-mode))

;; Scheme
(use-package geiser
  :hook scheme-mode
  :custom
  (geiser-default-implementation 'gauche)
  ;; limit choices to installed implementations
  ;; full list: '(chez chibi chicken gambit gauche guile kawa mit racket stklos)
  (geiser-active-implementations '(chibi gauche guile mit))
  (geiser-repl-query-on-kill-p nil)
  (geiser-mode-start-repl-p t)
  (indent-tabs-mode nil))
(use-package geiser-mit
  :after geiser
  :commands geiser-mit)
(use-package geiser-guile
  :after geiser
  :commands geiser-guile)
(use-package geiser-chibi
  :after geiser
  :commands geiser-chibi)
(use-package geiser-gauche
  :after geiser
  :commands geiser-gauche
  :custom (geiser-gauche-extra-command-line-parameters '("-i")))

;; Common Lisp
(use-package slime
  :bind ("C-z" . 'slime-selector)
  :config
  (setq slime-lisp-implementations
        '((sbcl  ("/usr/bin/sbcl" "--dynamic-space-size" "2GB"))
          (ecl   ("/usr/bin/ecl"))))
  :custom
  (slime-net-coding-system 'utf-8-unix)
  :preface
  (defun run-slime ()
    (save-excursion
      (slime)
      (when (= (count-windows) 2)
        (window-swap-states))))
  :hook (lisp-mode . run-slime))

;; Ninja build system
(use-package ninja-mode
  :mode "build\\.ninja")

;; Rust mode
(use-package rust-mode
  :mode "\\.rs\\'")

;; Go mode
(use-package go-mode
  :mode "\\.go\\'"
  :custom (tab-width 2))

;; Forth mode
(use-package forth-mode
  :mode "\\.fs\\'"
  :defines forth-executable
  :config
  (require 'forth-block-mode)
  (require 'forth-interaction-mode)
  (setq forth-executable "gforth"))

;; Lua mode
(use-package lua-mode
  :mode "\\.lua\\'")

;; OCaml
(use-package tuareg
  :mode (("\\.ml\\'" . tuareg-mode)
         ("\\.ocamlinit\\'" . tuareg-mode)))

(use-package dune
  :mode (("dune" . dune-mode)
         ("dune-project" . dune-mode)))

(use-package merlin
  :config
  (setq merlin-error-after-save nil)  ; we're using flycheck instead
  :hook ((tuareg-mode . merlin-mode)
         (merlin-mode . company-mode)))

(use-package merlin-eldoc
  :hook (tuareg-mode . merlin-eldoc-setup))

(use-package flycheck-ocaml
  :hook (tuareg-mode . flycheck-ocaml-setup))

(use-package utop
  :hook (tuareg-mode . utop-minor-mode))

;; Always fun to keep the software bible close
(use-package sicp)

;; Markdown
(use-package markdown-mode
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :hook (markdown-mode . turn-on-auto-fill)
  :custom
  (markdown-command '("pandoc"
                      "--standalone"
                      "--from=markdown"
                      "--to=html5"
                      "--katex"
                      "--highlight-style=pygments"))
  (markdown-command-needs-filename nil)
  (markdown-enable-prefix-prompts nil)
  (markdown-asymmetric-header t)
  (markdown-enable-math t))

;; Octave
(use-package octave
  :mode ("\\.m\\'" . octave-mode))

;; YAML
(use-package yaml-mode
  :mode ("\\.yml\\'" "\\.yaml\\'"))

;; JSON
(use-package json-mode
  :mode "\\.json\\'")

;; Dockerfile
(use-package dockerfile-mode
  :mode "Dockerfile\\'")

;; C/C++/Awk/Java/... modes
(use-package cc-mode
  :mode (("\\.c\\'" . c-mode)
         ("\\.h\\'" . c-or-c++-mode)
         ("\\.cpp\\'" . c++-mode)
         ("\\.cc\\'" . c++-mode)
         ("\\.awk\\'" . awk-mode)
         ("\\.java\\'" . java-mode))
  :config
  (setq c-default-style '((java-mode . "java")
                          (awk-mode  . "awk")
                          (other     . "linux"))
        c-basic-offset 2
        c-tab-always-indent t
        indent-tabs-mode nil)
  :custom
  (gdb-many-windows 1))

;; Language Server Protocol
(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook (lsp-mode . lsp-enable-which-key-integration)
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config
  (lsp-modeline-code-actions-mode 1)
  :custom
  (lsp-enable-on-type-formatting nil)
  :delight " LSP")

(use-package lsp-ui       :commands lsp-ui-mode)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)
(use-package dap-mode     :commands (dap-debug dap-debug-last))

;; LSP support for C/C++ modes
(use-package ccls
  :hook ((c-mode c++mode) . lsp-deferred))

(use-package platformio-mode
  :commands platformio-mode
  :hook ((c-mode c++-mode) . platformio-conditionally-enable))

;; Dart and language server integration
(use-package dart-mode
  :mode "\\.dart\\'")

(use-package lsp-dart
  :custom
  (lsp-dart-sdk-dir "/opt/dart-sdk")
  (lsp-dart-line-length 96)
  (lsp-dart-dap-flutter-hot-reload-on-save t)
  :hook (dart-mode . lsp-deferred))

;; Haskell
(use-package haskell-mode
  :mode (("\\.hs\\'" . haskell-mode)
         ("\\.lhs\\'" . haskell-literate-mode))
  :hook (haskell-mode . interactive-haskell-mode))

(use-package lsp-haskell
  :hook ((haskell-mode haskell-literate-mode) . lsp-deferred))

;;; develop.el ends here
